# Projeto de testes unitários

<br>
<figure>
    <img src="https://cursos.alura.com.br/assets/images/alura/logo-alura.png" alt="Certificado"  width="50" height="25">
    <figcaption><b>Certificado de conclusão</b></figcaption>
</figure>

## Aprendizados

- Uso de dados inline ou via classe de dados (Theory vs fact)
- Padronização de nomenclaturas
- "First Test" orientar implementação partindo do teste
- Tratamentos de exceções (Validando exceções)
- Asserts base: equals, contains e null
- Quando um teste gera efetivamente valor
- Regressões da aplicação através da validação dos testes
