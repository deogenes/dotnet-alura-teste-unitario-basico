using Alura.Estacionamento.Modelos;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Alura.Estacionamento.Tests;

public class VeiculoTest
{
    private ITestOutputHelper _testOutputHelper;

    public VeiculoTest(
        ITestOutputHelper testOutputHelper
    )
    {
        _testOutputHelper = testOutputHelper;
    }

    //Test pattern AAA
    [Fact]
    [Trait("Funcionalidade", "Acelerar")]
    public void AcelerarEmDez()
    {
        //Arrange
        var veiculo = new Veiculo();

        //Act
        veiculo.Acelerar(10);

        _testOutputHelper.WriteLine("Acelerando em 10 o veiculo");

        //Assert
        Assert.Equal(100, veiculo.VelocidadeAtual);
    }

    [Fact]
    [Trait("Funcionalidade", "Frear")]
    public void TestaVeiculoFrear()
    {
        //Arrange
        var veiculo = new Veiculo();

        //Act
        veiculo.Frear(10);
        
        //Assert
        Assert.Equal(-150, veiculo.VelocidadeAtual);
    }

    [Fact(Skip = "Test ainda não implementado")]
    public void ValidaNomeProprietario()
    {
        
    }

    [Fact]
    public void FichaDadosDoVeiculo()
    {
        //Arrange
        var veiculo = new Veiculo()
        {
            Proprietario = "Fulano da silva",
            Tipo = TipoVeiculo.Automovel,
            Cor = "Azul",
            Modelo = "Gol",
            Placa = "BLU-0800"
        };

        //Act
        var dados = veiculo.ToString();

        //Assert
        Assert.Contains("Tipo do Veículo: Automovel", dados);
    }

    [Fact]
    public void NomeProprietarioVeiculoMenosCaracteres()
    {
        var handle = () => new Veiculo() { Proprietario = "De" };
        
        var exception = Assert.Throws<FormatException>(handle);
    }
}