﻿
using Alura.Estacionamento.Modelos;

namespace Alura.Estacionamento.Tests
{
    public class PatioTest
    {
        [Fact(DisplayName = "Valida Faturamento")]
        public void ValidaFaturamento()
        {
            //Arrange
            var estacionamento = new Patio()
            {
                OperadorPatio = new Operador()
                {
                    Nome = "Operador fulano nogueira"
                }
            };

            var veiculo = new Veiculo()
            {
                Proprietario = "André Silva",
                Tipo = TipoVeiculo.Automovel,
                Cor = "Verde",
                Modelo = "Fusca",
                Placa = "asd-9999"
            };

            estacionamento.RegistrarEntradaVeiculo(veiculo);
            estacionamento.RegistrarSaidaVeiculo(veiculo.Placa);

            //Act
            double faturamento = estacionamento.TotalFaturado();

            //Assert
            Assert.Equal(2, faturamento);
        }

        [Theory]
        [ClassData(typeof(VeiculosData))]
        public void ValidaFaturamentoComVariosVeiculos(Veiculo veiculo)
        {
            //Arrange
            var estacionamento = new Patio()
            {
                OperadorPatio = new Operador()
                {
                    Nome = "Operador fulano nogueira"
                }
            };

            estacionamento.RegistrarEntradaVeiculo(veiculo);
            estacionamento.RegistrarSaidaVeiculo(veiculo.Placa);

            //Act
            double faturamento = estacionamento.TotalFaturado();

            //Assert
            Assert.Equal(2, faturamento);
        }

        [Theory(DisplayName = "Pesquisa veiculo no patio")]
        [ClassData(typeof(VeiculosData))]
        public void ValidaPesquisaVeiculoPatio(Veiculo veiculo)
        {
            //Arrange
            var estacionamento = new Patio()
            {
                OperadorPatio = new Operador()
                {
                    Nome = "Operador fulano nogueira"
                }
            };

            estacionamento.RegistrarEntradaVeiculo(veiculo);
            var resultado = estacionamento.PesquisaVeiculoPorPlaca(veiculo.Placa);
            Assert.Equal(resultado.Placa, veiculo.Placa);

            estacionamento.RegistrarSaidaVeiculo(veiculo.Placa);
            resultado = estacionamento.PesquisaVeiculoPorPlaca(veiculo.Placa);

            Assert.Null(resultado);
        }

        [Fact]
        public void GerarNovoTicket()
        {
            var veiculo = new Veiculo()
            {
                Proprietario = "André Silva",
                Tipo = TipoVeiculo.Automovel,
                Cor = "Verde",
                Modelo = "Fusca",
                Placa = "asd-9999"
            };

            var estacionamento = new Patio()
            {
                OperadorPatio = new Operador()
                {
                    Nome = "Operador fulano nogueira"
                }
            };

            estacionamento.RegistrarEntradaVeiculo(veiculo);

            Assert.Contains("### Ticket Estacionameno Alura ###", veiculo.Ticket);
            Assert.Contains($"Identificador: {veiculo.IdTicket}", veiculo.Ticket);
        }
    }

    public class VeiculosData : TheoryData<Veiculo>
    {
        public VeiculosData()
        {
            Add(new Veiculo()
            {
                Proprietario = "Fulano da silva",
                Tipo = TipoVeiculo.Automovel,
                Cor = "Azul",
                Modelo = "Gol",
                Placa = "BLU-0800"
            });

            Add(new Veiculo()
            {
                Proprietario = "Deltrano da silva",
                Tipo = TipoVeiculo.Automovel,
                Cor = "Azul",
                Modelo = "Gol",
                Placa = "GLA-0800"
            });
        }
    }

}
